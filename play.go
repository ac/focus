package main

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"os"

	"catinello.eu/x/mp3"
	"catinello.eu/x/oto"
)

// play() is used in parallel via go call. Errors will be printed to stderr and
// cause an abort with error code 3 on sound related issues or 4 if player related.
func play(p *oto.Player, category int) {
	var f *bytes.Reader
	var err error

	switch category {
	case signal:
		c, err := base64.StdEncoding.DecodeString(yourturn)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(3)
		}
		f = bytes.NewReader(c)
	case begin:
		c, err := base64.StdEncoding.DecodeString(appointed)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(3)
		}
		f = bytes.NewReader(c)
	case end:
		c, err := base64.StdEncoding.DecodeString(unconvinced)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(3)
		}
		f = bytes.NewReader(c)
	case tick:
		c, err := base64.StdEncoding.DecodeString(when)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(3)
		}
		f = bytes.NewReader(c)
	}

	d, err := mp3.NewDecoder(f)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(4)
	}

	if _, err := io.Copy(p, d); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(4)
	}
}
