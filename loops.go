// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"time"

	"catinello.eu/x/oto"
)

// warmup is a simple countdown with tick sounds every second.
func (s *session) warmup(player *oto.Player) {
	ticker := time.NewTicker(time.Second * 1)
	start := time.Now()

	if prelude > 0 {
		for {
			elapsed := time.Since(start)
			if elapsed <= prelude {
				s.output("Warmup", (prelude - elapsed).Round(time.Second))
				if !s.silent {
					go play(player, tick)
				}
				<-ticker.C
			} else {
				null, _ := time.ParseDuration("0s")
				s.output("Warmup", null)
				break
			}
		}
		println()
	}

	ticker.Stop()
	return
}

// core runs the clock and plays sounds on every half time defined.
func (s *session) core(off chan<- bool, command chan int, full time.Duration, player *oto.Player) {
	var state = stateStart

	per := (full / 100) * 10
	rest := full
	alert := full / 2

	ticker := time.NewTicker(interval)

	s.m.D().Println("play sound begin")
	if !s.silent {
		play(player, begin)
	}

	var elapsed, elapsedSig time.Duration

	for {
		select {
		case cmd := <-command:
			s.m.D().Println(cmd)
			switch cmd {
			case stateStop:
				state = stateStop
				off <- true
				break
			case statePause:
				state = statePause
				continue
			default:
				state = stateStart
			}
		default:
			switch state {
			case stateStart:
				elapsed += interval
				s.m.D().Println(elapsed)
				if elapsed >= full || alert <= interval {
					ticker.Stop()
					s.m.D().Println("play sound end")
					if !s.silent {
						play(player, end)
					}
					s.m.Printf("\r\n")
					off <- true
					return
				}

				elapsedSig += interval
				if elapsedSig >= alert && alert > time.Second*2 {
					if alert > per || s.optout {
						s.m.D().Println("play sound signal")
						if !s.silent {
							if !s.tickless {
								go play(player, signal)
							}
						}
					}

					elapsedSig = 0
					rest = rest - alert
					alert = rest / 2
					continue
				} else {
					t := <-ticker.C

					s.m.D().Println(t.String())
					s.output("Elapsed", elapsed.Round(time.Second))
					continue
				}
			case statePause:
				time.Sleep(interval)
				continue
			}
		}
	}
}
