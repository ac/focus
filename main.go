// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	_ "embed"
	"os"
	"strconv"
	"strings"
	"time"

	"catinello.eu/com"
	"catinello.eu/restrict"
	"catinello.eu/x/oto"

	"golang.org/x/term"
)

const (
	// kinds of sounds
	signal = iota
	begin
	end
	tick
)

const (
	stateStart = iota
	statePause
	stateStop
)

var (
	// default parameters
	interval time.Duration = time.Millisecond * 500
	prelude  time.Duration

	version string
)

//go:embed LICENSE
var license string

type session struct {
	m        *com.Config
	optout   bool
	silent   bool
	tickless bool
}

// default output
func (s *session) output(t string, e time.Duration) {
	s.m.Printf("\033[2K\r%v: %v", t, e)
}

func main() {
	var full time.Duration
	var err error
	var m *com.Config

	if len(os.Getenv("DEBUG")) > 0 {
		m = com.New(com.Debug)
	} else {
		m = com.New(com.Common)
	}

	m.D().Println("DEBUG: ", os.Getenv("DEBUG"))

	if err := restrict.Syscalls("audio stdio cpath rpath wpath unix unveil"); err != nil {
		m.E().Println(err)
		os.Exit(255)
	}

	if err := restrict.Access("/", "r"); err != nil {
		m.E().Println(err)
		os.Exit(255)
	}

	id := os.Geteuid()
	strid := strconv.Itoa(id)

	if err := restrict.Access("/run/user/"+strid, "crw"); err != nil {
		m.E().Println(err)
		os.Exit(255)
	}

	if err := restrict.AccessLock(); err != nil {
		m.E().Println(err)
		os.Exit(255)
	}

	s := session{m: m}

	if len(os.Args) > 1 {
		m.D().Println(os.Args[1:])
		for _, v := range os.Args[1:] {
			switch true {
			case v == "--help":
				help()
				os.Exit(0)
			case v == "--version":
				m.Println(version)
				os.Exit(0)
			case v == "--license":
				m.Print(license)
				os.Exit(0)
			case v == "--tickless":
				s.tickless = true
				m.D().Println("TICKLESS: ", s.tickless)
			case strings.HasPrefix(v, "--warmup="):
				prelude, err = time.ParseDuration(strings.Split(v, "=")[1])
				if err != nil {
					m.E().Println("WARMUP: " + err.Error())
					os.Exit(1)
				}
				m.D().Println("WARMUP: ", prelude)
			default:
				if strings.HasPrefix(v, "-") {
					m.E().Println("Unknown option.")
					help()
					os.Exit(1)
				}

				full, err = time.ParseDuration(v)
				if err != nil {
					m.E().Println(err)
					os.Exit(1)
				}

				if full < time.Second*2 {
					m.E().Println("Duration is too small.")
					os.Exit(1)
				}
			}
		}
	} else {
		help()
		os.Exit(0)
	}

	if len(os.Getenv("INTERVAL")) > 0 {
		var err error
		interval, err = time.ParseDuration(os.Getenv("INTERVAL"))
		if err != nil {
			m.E().Println(err)
			os.Exit(1)
		}

		if interval > time.Second*1 {
			m.E().Println("INTERVAL: Interval is too big.")
			os.Exit(1)
		}
	}

	m.D().Println("INTERVAL: ", interval)

	if len(os.Getenv("OPTOUT")) > 0 {
		s.optout = true
	}

	m.D().Println("OPTOUT: ", s.optout)

	if len(os.Getenv("SILENT")) > 0 {
		s.silent = true
	}

	m.D().Printf("Interval: %v\n", interval)
	s.dry(full)

	c, err := oto.NewContext(44100, 2, 2, 8192)
	defer c.Close()
	if err != nil {
		m.E().Println(err)
		os.Exit(1)
	}

	if err := restrict.Syscalls("audio stdio unix"); err != nil {
		m.E().Println(err)
		os.Exit(255)
	}

	p := c.NewPlayer()

	s.warmup(p)

	signal := make(chan int)
	off := make(chan bool, 1)
	go s.core(off, signal, full, p)

	ts, err := term.MakeRaw(int(os.Stdin.Fd()))
	defer term.Restore(int(os.Stdin.Fd()), ts)
	if err != nil {
		m.E().Println(err)
		os.Exit(1)
	}

	go func(s chan<- int, m *com.Config) {
		var pauseSwitch bool

		for {
			b := make([]byte, 1)
			_, err = os.Stdin.Read(b)
			if err != nil {
				m.E().Println(err)
				os.Exit(3)
			}

			// first hit of space to pause
			if b[0] == 32 && pauseSwitch == false {
				pauseSwitch = true
				signal <- statePause
				m.Printf("\r[PAUSE]")
				time.Sleep(interval)
				continue
			}

			// second hit to resume
			if b[0] == 32 && pauseSwitch {
				pauseSwitch = false
				signal <- stateStart
				continue
			}

			// quit with q/Q key press
			if b[0] == 81 || b[0] == 113 {
				signal <- stateStop
				m.Printf("\r\n")
				break
			}

			// show quick help
			if b[0] == 72 || b[0] == 104 {
				m.Printf("\r\nKeys: [h for this help] [q to quit] [space to pause/resume] [return to set a mark]\r\n")
				continue
			}

			// set mark
			if b[0] == 13 {
				m.Printf("\r\n -------\r\n")
				continue
			}
		}
	}(signal, m)

	<-off
	close(off)
	close(signal)

	err = p.Close()
	if err != nil {
		m.E().Println(err)
		os.Exit(2)
	}
}
