// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"time"
)

func (s *session) dry(d time.Duration) {
	full := d
	var counter uint8

	var (
		green string = "\033[0;32m"
		red   string = "\033[0;31m"
		blue  string = "\033[0;34m"
		end   string = "\033[0m"
	)

	s.m.Printf("Sounds at: %s%s%s ", green, "0s", end)

	per := (d / 100) * 10

	for {
		d = d / 2
		if d > time.Second*2 {
			if d > time.Duration(per) || s.optout {
				s.m.Printf("%s%v%s ", blue, (full - d).Round(interval), end)
				counter++
			}
		} else {
			s.m.Printf("%s%v%s [%v] | Press h for help.\n", red, full, end, counter+2)
			break
		}
	}
}
