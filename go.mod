module focus

go 1.21.0

require (
	catinello.eu/com v0.0.0-20230605184819-5ef4b6d000e5
	catinello.eu/restrict v0.0.0-20230916094435-88b3fa274bea
	catinello.eu/x/mp3 v0.0.0-20220811071212-aff725f500f1
	catinello.eu/x/oto v0.0.0-20220811071212-aff725f500f1
	golang.org/x/term v0.12.0
)

require (
	github.com/seccomp/libseccomp-golang v0.10.0 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/exp/shiny v0.0.0-20230905200255-921286631fa9 // indirect
	golang.org/x/image v0.12.0 // indirect
	golang.org/x/mobile v0.0.0-20230906132913-2077a3224571 // indirect
	golang.org/x/sys v0.12.0 // indirect
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.69 // indirect
)
